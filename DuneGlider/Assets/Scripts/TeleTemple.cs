﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleTemple : MonoBehaviour {
    public string DuneGliderTemple;

    // Use this for initialization
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(DuneGliderTemple);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
