﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]

public class PlayerMove : MonoBehaviour {

    public float speed;


    [SerializeField]
    private Vector2 deltaForce;

    private Rigidbody2D RGB;

    private PolygonCollider2D BX2D;

    private void Awake()
    {
        RGB = GetComponent<Rigidbody2D>();
        BX2D = GetComponent<PolygonCollider2D>();
    }
    // Use this for initialization
    void Start ()
    {
        RGB.gravityScale = 0;
        RGB.constraints = RigidbodyConstraints2D.FreezeRotation;
	}

    // Update is called once per frame
    void Update ()
    {
        CheckInput();

        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Vector3 bck = transform.TransformDirection(Vector3.back);
        Vector3 lft = transform.TransformDirection(Vector3.left);
        Vector3 rgt = transform.TransformDirection(Vector3.right);

        if (Physics.Raycast(transform.position, fwd, 0.01f))
        {
            speed = 0;
        }
        else if (Physics.Raycast(transform.position, bck, 0.1f))
        {
            speed = 0;
        }
        else if (Physics.Raycast(transform.position, lft, 0.1f))
        {
            speed = 0;
        }
        else if (Physics.Raycast(transform.position, rgt, 0.1f))
        {
            speed = 0;
        }
    }

    void CheckInput()
    {
        var H = Input.GetAxisRaw("Horizontal");
        var V = Input.GetAxisRaw("Vertical");

        deltaForce = new Vector2(H, V);

        var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        transform.position += move * speed * Time.deltaTime;

        

    }

}
