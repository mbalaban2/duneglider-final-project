﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OasisKill : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Fire")

        {
            Destroy(col.gameObject);
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
