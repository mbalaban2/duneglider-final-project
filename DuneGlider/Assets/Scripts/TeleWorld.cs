﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleWorld : MonoBehaviour
{
    public string DuneWorld;

    // Use this for initialization
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(DuneWorld);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
