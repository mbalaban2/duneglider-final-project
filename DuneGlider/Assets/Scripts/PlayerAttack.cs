﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Fire")
        {
            if (Input.GetButton("Jump"))
            {  
                Destroy(col.gameObject);
            }
        }
    }

    void Update()
    {
    }

}
