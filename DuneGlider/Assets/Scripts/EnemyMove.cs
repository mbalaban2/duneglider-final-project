﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {
    public GameObject target;
    public Transform player;
    public float smoothTime = 3f;
    private Vector3 smoothVelocity = Vector3.zero;




    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, target.transform.position, ref smoothVelocity, smoothTime);
    }
}
