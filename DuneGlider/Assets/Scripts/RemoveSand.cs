﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveSand : MonoBehaviour {

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Sand")
        {
            if (Input.GetButton("Jump"))
            {
                Destroy(col.gameObject);
            }
        }
        
    }

    /*void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Sand")
        {
            if (Input.GetButton("Jump"))
            {
                Destroy(col.gameObject);
            }
        }
           
        
    }*/

    // Update is called once per frame
    void Update () {
		
	}
}
